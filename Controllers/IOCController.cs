using asp_vnext_ioc.Interfaces;
using Microsoft.AspNet.Mvc;

namespace asp_vnext_ioc.Controllers
{
	[Route("api/[controller]")]
	public class IOCController : Controller
	{
		private IWidget _widget;
		
		public IOCController(IWidget widget)
		{
			_widget = widget;
		}
		
		[HttpGet]
		public string Get()
		{
			return _widget.DoWidgetThings();
		}
	}
	
}