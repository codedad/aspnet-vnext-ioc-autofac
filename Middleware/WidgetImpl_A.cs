using asp_vnext_ioc.Interfaces;

namespace asp_vnext_ioc
{
    public class WidgetImpl_A : IWidget
    {
        public string DoWidgetThings()
        {
            return "This is my Widget Implementation!";
        }
    }
}