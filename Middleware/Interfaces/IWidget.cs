namespace asp_vnext_ioc.Interfaces
{
	public interface IWidget
	{
		string DoWidgetThings();
	}
}